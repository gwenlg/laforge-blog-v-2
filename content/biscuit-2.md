+++
title = "En route vers Biscuit (Partie 2)"
date = 2022-09-01
draft = false
template  = 'post-with-biscuit.html'

[taxonomies]
categories = ["Biscuit"]
tags = ["sécurité", "biscuit"]

[extra]
lang = "fr"
toc = true
show_comment = true
math = false
mermaid = false
cc_license = true
outdate_warn = true
outdate_warn_days = 120
+++

test23456

{% biscuit(enabled=["blocks", "add_block", "authorizer", "result", "third_party", "custom_external"]) %}
  <code class="block">user(12)</code>
  <code class="block">check if user(12)</code>
  <code class="block">check if user(12)</code>
  <code class="authorizer">check if user(12);
allow if true</code>
{% end %}

----

{% biscuit(enabled=["authorizer"]) %}
  <code class="block">user(12)</code>
  <code class="block">check if user(12)</code>
  <code class="block">check if user(12)</code>
  <code class="authorizer">check if user(12);
allow if true</code>
{% end %}

----

{% biscuit(enabled=["authorizer", "blocks"]) %}
  <code class="block">user(12);
name("nahis");</code>
  <code class="authorizer">check if user(12);
check if name($u), $u == "nahis";
allow if true</code>
{% end %}



[https://github.com/codeandmedia/zola_easydocs_theme/blob/master/static/js.js](https://github.com/codeandmedia/zola_easydocs_theme/blob/master/static/js.js#L232)