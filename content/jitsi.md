+++
title = "Installer Jitsi"
date = 2021-03-06
draft = false

[taxonomies]
categories = ["SysAdmin"]
tags = ["linux", "tools"]

[extra]
lang = "fr"
toc = true
show_comment = true
math = false
mermaid = false
cc_license = true
outdate_warn = true
outdate_warn_days = 120
+++

<p align="center">
  <img src="https://media.giphy.com/media/jV4wbvtJxdjnMriYmY/giphy.gif" alt=""/>
</p>


Aujourd'hui petit tuto pour installer sa propre instance de Jitsi.

Selon Wikipedia:

[Jitsi](https://meet.jit.si/) est une alternative libre aux applications de téléphone et téléconférence en termes de fonctionnalités.

C'est parti pour l'installation 😎.

## Prérequis

L'Installation sera réalisée sur une Ubuntu 20.04

On met à jour le cache apt et on installe les mises à jour des différents paquets.

```bash
apt update && apt upgrade
```

Jitsi utilise java pour fonctionner vous devez donc installer soit
openJDK8 ou openJDK11.

```bash
apt install openjdk-11-jre-headless
#ou
apt install openjdk-8-jre-headless
```

Vérifier que java est correctement installé et à la bonne version

```bash
$ java -version
openjdk version "11.0.10" 2021-01-19
OpenJDK Runtime Environment (build 11.0.10+9-Ubuntu-0ubuntu1.20.04)
OpenJDK 64-Bit Server VM (build 11.0.10+9-Ubuntu-0ubuntu1.20.04, mixed mode, sharing)
```

Le paquet gnupg2 est nécessaire pour la gpg de Jitsi.

```bash
apt install gnupg2
```

Nginx va servir de reverse proxy et de terminaison TLS.

```bash
apt install nginx-full
```

Ajout du dépôt de paquets Jitsi

```bash
curl https://download.jitsi.org/jitsi-key.gpg.key | sudo sh -c 'gpg --dearmor > /usr/share/keyrings/jitsi-keyring.gpg'
echo 'deb [signed-by=/usr/share/keyrings/jitsi-keyring.gpg] https://download.jitsi.org stable/' | sudo tee /etc/apt/sources.list.d/jitsi-stable.list > /dev/null
```

Puis on remet à jour le cache apt

```bash
apt update
```

## Nom de domaine

Vous devez posséder un nom de domaine et faire pointer un enregistrement (A ou CNAME) vers le serveur qui hébergera Jitsi.

Par exemple mon nom de domaine sera `jitsi.example.com`

Pour permettre au serveur de lui même se connaître sous ce nom il est possible de modifier le FQDN du serveur.

```bash
hostnamectl set-hostname jitsi.example.com
```

Et de modifier aussi dans le fichier `/etc/hosts`

```
x.x.x.x jitsi.example.com 
```

où `x.x.x.x` est l'IP publique du serveur.

Pour vérifier que tout est fonctionel.

```bash 
ping "$(hostname)"
```

## Firewall

Plusieurs ports doivent être ouverts pour le bon fonctionnnement de Jitsi.

```bash
ufw allow 80/tcp # renouvellement des certificats let's encrypt
ufw allow 443/tcp # interface web
ufw allow 10000/udp # canal principal du flux vidéo
ufw allow 22/tcp # ssh
ufw allow 3478/udp # port du STUN
ufw allow 5349/tcp # fallback du flux vidéo
```

[STUN](https://en.wikipedia.org/wiki/STUN). 

Vérifié bien avant que le port 22 (ou celui que vous utilisez pour vous connecter en SSH) est bien autorisé, sinon
vous allez vous enfermer dehors et pleurer! 😱

```bash
ufw enable
```

## Installation de Jitsi lui même

```bash
apt install jitsi-meet
```

Il va vous demander un nom de domaine, mettez le FQDN de votre serveur.

Il vous faut aussi un certificat pour la terminaison TLS.

Pour cela il suffit de lancer le script :

```bash
/usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh
```

Il va vous demander un email, ensuite vous pouvez valider avec la touche "entrée".

Une fois la procédure d'installation du certificat terminée.

Vous pouvez vous rendre sur `jitsi.example.com` (remplacer par votre FQDN)

Et vous devriez voir

![](../assets/images/jitsi/home.png)

Et voilà vous avez votre propre instance de Jitsi d'installée, à vous les réunions de 24h à 25 participants ( plz kill me 😖).

## Conclusion

J'espère que ce mini article pourra vous aider.

A la prochaine fois pour autre chose 😁